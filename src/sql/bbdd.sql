create database RRHH;
use RRHH;

 create table empleados (
     ID_empleado int (8) auto_increment primary key,
     Nombre char (30) not null,
     Apellidos char (50) not null,
     FechaNacimiento date,
     Sexo enum('Hombre','Mujer') default null,
     Departamento enum('Administración', 'Ventas', 'Márketing', 'Tecnología') default null,
     FechaContratación date
 );

 Insert into empleados values (1, 'Celia', 'Casado Pablos', '1995-08-27', 'Mujer', 'Tecnología', '2019-06-07');

