const dbConnection = require('../../config/dbconnect');

module.exports = app => {

    const connection = dbConnection();

    app.get('/empleados', (req, res) => {
        connection.query('SELECT * FROM empleados', (err, result) => {
            res.render('empleados', {
                form: result
            });
        });
    });

    app.post('/new-empleado', (req, res) => { console.log(req.body);
        const { Nombre, Apellidos, FechaNacimiento, Sexo, Departamento, FechaContratación } = req.body;
        connection.query('INSERT INTO empleados SET ? ',
            {
                Nombre,
                Apellidos,
                FechaNacimiento,
                Sexo,
                Departamento,
                FechaContratación
            }
            , (err, result) => {
                res.redirect('/empleados');
            });
    });
};
