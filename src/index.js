const app = require('./config/server');

require('./app/routes/form')(app);

app.listen(3000);
