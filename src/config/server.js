const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../app/vistas'));
app.use(bodyParser.urlencoded({extended: false}));

module.exports = app;
